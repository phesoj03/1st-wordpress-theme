<?php get_header(); ?>

<div class="container content-container">
	<div class="row">
		<div class="col-md-9">
			<?php
				if ( have_posts() ) :

					?>
					<div class="content-container">
						<?php
						while ( have_posts() ) {
							the_post();
							get_template_part( 'content', get_post_format() );
						}
						?>
					</div>
					<?php
				else :
					
					echo '<p>There are no posts!</p>';
				 
				endif;
			?>
		</div>
		<div class="col-md-3">
			<?php if ( is_active_sidebar( 'rightsidebar' ) ) { ?>
	        <div class="sidebar-column"><!-- sidebar-column -->
				<?php dynamic_sidebar( 'rightsidebar' ) ?>
	        </div><!-- sidebar-column -->
		<?php } ?>
		</div>
	</div>
</div>

<?php get_footer(); ?>