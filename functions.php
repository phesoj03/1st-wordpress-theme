<?php
if ( ! file_exists( get_template_directory() . '/class-wp-bootstrap-navwalker.php' ) ) {
	// file does not exist... return an error.
	return new WP_Error( 'class-wp-bootstrap-navwalker-missing', __( 'It appears the class-wp-bootstrap-navwalker.php file may be missing.', 'wp-bootstrap-navwalker' ) );
} else {
	// file exists... require it.
	require_once get_template_directory() . '/class-wp-bootstrap-navwalker.php';
}

function custom_theme_assets() {
	wp_deregister_script('jquery');
	wp_enqueue_style( 'style', get_stylesheet_uri() );
	//css
	wp_enqueue_style('bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css', array(), '4.1.0', 'all');
	wp_enqueue_style('mdbcss', get_template_directory_uri() . '/css/mdb.min.css', array(), '4.5.4', 'all');
	wp_enqueue_style('fontawesome', get_template_directory_uri() . '/css/fontawesome-all.min.css', array(), '5.0.10', 'all');
	wp_enqueue_style( 'custom-google-fonts1', "https://fonts.googleapis.com/css?family=Raleway:300,400,600", array(), 'fonts1', 'all' );
	wp_enqueue_style( 'custom-google-fonts2', "https://fonts.googleapis.com/css?family=Montserrat", array(), 'fonts2', 'all' );
	wp_enqueue_style( 'site', get_template_directory_uri() . '/css/site.css', array(), 'sitev1', 'all' );
	//js
	wp_enqueue_script('jquery', get_template_directory_uri() . '/js/jquery-3.3.1.min.js', array(), '3.3.1', true);
	wp_enqueue_script('popper', get_template_directory_uri() . '/js/popper.min.js', array(), '2018', true);
	wp_enqueue_script('bootstrapjs', get_template_directory_uri() . '/js/bootstrap.min.js', array(), '4.1.0', true);
	wp_enqueue_script('mdbjs', get_template_directory_uri() . '/js/mdb.min.js', array(), '4.5.4', true);
	wp_enqueue_script('scrollreveal', get_template_directory_uri() . '/js/scrollreveal.min.js', array(), '3.4.0', true);
	wp_enqueue_script('sitejs', get_template_directory_uri() . '/js/site.js', array(), '1.0', true);
}
 
add_action( 'wp_enqueue_scripts', 'custom_theme_assets' );
register_nav_menus( [ 'primary' => __( 'Primary Menu' ) ] );

/* Add Featured Image Support To Your WordPress Theme */
function add_featured_image_support_to_your_wordpress_theme() {
	add_theme_support( 'post-thumbnails' );
	add_image_size( 'small-thumbnail', 100, 100, true );
	add_image_size( 'single-post-image', 250, 250, true );
	add_theme_support( 'post-formats', [
		'aside',
		'gallery',
		'link',
		'image',
		'quote',
		'status',
		'video',
		'audio',
		'chat',
		'standard'
	] );
}
 
add_action( 'after_setup_theme', 'add_featured_image_support_to_your_wordpress_theme' );

function initialize_widgets() {
	register_sidebar( [
		'name'          => 'Right Sidebar',
		'id'            => 'rightsidebar',
		'before_widget' => '<div class="widget-item">',
		'after_widget'  => '</div>'
	] );
	
	register_sidebar( [
		'name'          => 'Footer',
		'id'            => 'footer',
		'before_widget' => '<div class="widget-item">',
		'after_widget'  => '</div>'
	] );
}
 
add_action( 'widgets_init', 'initialize_widgets' );

function get_the_top_ancestor_id() {
	global $post;
	if ( $post->post_parent ) {
		$ancestors = array_reverse( get_post_ancestors( $post->ID ) );
		return $ancestors[0];
	} else {
		return $post->ID;
	}
}

add_filter('nav_menu_css_class' , 'wpsites_nav_class' , 10 , 2);

function wpsites_nav_class($classes, $item){

if( !is_page() && 'All Post' == $item->title){     

         $classes[] = "active";
 }
 return $classes;
}