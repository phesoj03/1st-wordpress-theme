<article class="post">
	<div class="card card-article">
		
		<div class="card-body">
			<h5 class="card-title"><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h5>
			<p class="post-meta"><?php the_time( 'F jS, Y' ); ?> | <a
	                href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_nicename' ) ); ?>"><?php the_author(); ?></a>
		        | <?php
				$categories = get_the_category();
				$comma      = ', ';
				$output     = '';
				
				if ( $categories ) {
					foreach ( $categories as $category ) {
						$output .= '<a href="' . get_category_link( $category->term_id ) . '">' . $category->cat_name . '</a>' . $comma;
					}
					echo trim( $output, $comma );
				} ?>
		    </p>
			<p class="card-text">
			<?php
				the_content();
			?>
			</p>
			<?php if(!is_page()){ ?>
			<a href="<?php the_permalink() ?>" class="btn btn-primary">Read more &raquo</a>
			<?php } ?>
		</div>
		
	</div>
</article>