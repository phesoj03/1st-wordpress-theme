<?php get_header(); ?>

<div class="container-fluid front-page-banner">
	<div class="row">
		<div class="col-md-12 banner-text">
			<p>
				<span class="trans-box">My First Wordpress Theme!</span>
			</p>
		</div>
	</div>
</div>

<section class="services-container mb-100 mt-50">
	<div class="col-md-12">
		<div class="row">
			<div class="col-md-12 text-center mb-20">
				<span class="section-title">Our Services</span>
			</div>
		</div>	
	</div>
	<div class="container">
		<div class="row">
			<!--Card-->
			<div class="col-md-4 card-container">
				<div class="card testimonial-card">

				    <!--Bacground color-->
				    <div class="card-up indigo lighten-1">
				    </div>

				    <!--Avatar-->
				    <div class="avatar text-center card-icon">
				    	<span class="rounded-circle fab fa-html5"></span>
				    </div>

				    <div class="card-body">
				        <!--Name-->
				        <h4 class="card-title">Coding</h4>
				        <hr>
				        <!--Quotation-->
				        <p><i class="fa fa-quote-left"></i> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos, adipisci</p>
				    </div>

				</div>
			</div>
			<!--/.Card-->

			<!--Card-->
			<div class="col-md-4 card-container">
				<div class="card testimonial-card">

				    <!--Bacground color-->
				    <div class="card-up indigo lighten-1">
				    </div>

				    <!--Avatar-->
				    <div class="avatar text-center card-icon">
				    	<span class="rounded-circle fas fa-wrench"></span>
				    </div>

				    <div class="card-body">
				        <!--Name-->
				        <h4 class="card-title">Tools</h4>
				        <hr>
				        <!--Quotation-->
				        <p><i class="fa fa-quote-left"></i> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos, adipisci</p>
				    </div>

				</div>
			</div>
			<!--/.Card-->

			<!--Card-->
			<div class="col-md-4 card-container">
				<div class="card testimonial-card">

				    <!--Bacground color-->
				    <div class="card-up indigo lighten-1">
				    </div>

				    <!--Avatar-->
				    <div class="avatar text-center card-icon">
				    	<span class="rounded-circle fas fa-cog"></span>
				    </div>

				    <div class="card-body">
				        <!--Name-->
				        <h4 class="card-title">Support</h4>
				        <hr>
				        <!--Quotation-->
				        <p><i class="fa fa-quote-left"></i> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos, adipisci</p>
				    </div>

				</div>
			</div>
			<!--/.Card-->
		</div>
	</div>
</section>

<section class="team-section mb-100 mt-50">
	<div class="col-md-12">
		<div class="row">
			<div class="col-md-12 text-center mb-20">
				<span class="section-title">Our Team</span>
			</div>
		</div>	
	</div>
	<div class="container">
		<div class="row">
			<!-- Card -->
			<div class="col-md-4 card-container">
				<div class="card testimonial-card">

				    <!--Bacground color-->
				    <div class="card-up indigo blue-de-france">
				    </div>

				    <!--Avatar-->
				    <div class="avatar"><img src="<?php echo get_template_directory_uri(); ?>/imgs/1.jpg" class="rounded-circle">
				    </div>

				    <div class="card-body">
				        <!--Name-->
				        <h4 class="card-title">Back end/Front end Developer</h4>
				        <hr>
				        <!--Quotation-->
				        <p><i class="fa fa-quote-left"></i> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos, adipisci</p>
				    </div>

				</div>
			</div>
			<!-- Card -->

			<!--Card-->
			<div class="col-md-4 card-container">
				<div class="card testimonial-card">

				    <!--Bacground color-->
				    <div class="card-up indigo blue-de-france">
				    </div>

				    <!--Avatar-->
				    <div class="avatar"><img src="<?php echo get_template_directory_uri(); ?>/imgs/2.jpg" class="rounded-circle">
				    </div>

				    <div class="card-body">
				        <!--Name-->
				        <h4 class="card-title">UI/UX</h4>
				        <hr>
				        <!--Quotation-->
				        <p><i class="fa fa-quote-left"></i> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos, adipisci</p>
				    </div>

				</div>
			</div>
			<!--/.Card-->

			<!--Card-->
			<div class="col-md-4 card-container">
				<div class="card testimonial-card">

				    <!--Bacground color-->
				    <div class="card-up indigo blue-de-france">
				    </div>

				    <!--Avatar-->
				    <div class="avatar"><img src="<?php echo get_template_directory_uri(); ?>/imgs/3.jpg" class="rounded-circle">
				    </div>

				    <div class="card-body">
				        <!--Name-->
				        <h4 class="card-title">System Administrator/DBA</h4>
				        <hr>
				        <!--Quotation-->
				        <p><i class="fa fa-quote-left"></i> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos, adipisci</p>
				    </div>

				</div>
			</div>
			<!--/.Card-->
		</div>
	</div>
</section>
<?php get_footer(); ?>