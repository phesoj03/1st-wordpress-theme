<?php
 get_header();
?>
<div class="container content-container">
	<div class="row">
		<div class="col-md-9">
			<?php
			if ( have_posts() ) :
				while ( have_posts() ) : the_post(); ?>
					
					<article class="post content-container">
						<div class="card">
							<div class="card-body">
								<h5 class="card-title"><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h5>
								<p class="post-meta"><?php the_time( 'F jS, Y' ); ?> | <a
						                href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_nicename' ) ); ?>"><?php the_author(); ?></a>
							        | <?php
									$categories = get_the_category();
									$comma      = ', ';
									$output     = '';
									
									if ( $categories ) {
										foreach ( $categories as $category ) {
											$output .= '<a href="' . get_category_link( $category->term_id ) . '">' . $category->cat_name . '</a>' . $comma;
										}
										echo trim( $output, $comma );
									} ?>
							    </p>
								<p class="card-text">
								<?php
									the_post();
									// get_template_part( 'theposts', get_post_format() );
									the_content();
								?>
								</p>
							</div>
						</div>
					</article>

			 	<?php
			 	endwhile;
			else :
				echo '<p>There are no posts!</p>';
			 
			endif;
			?>
		</div>
		<div class="col-md-3">
			<?php if ( is_active_sidebar( 'rightsidebar' ) ) { ?>
	        <div class="sidebar-column"><!-- sidebar-column -->
				<?php dynamic_sidebar( 'rightsidebar' ) ?>
	        </div><!-- sidebar-column -->
		<?php } ?>
		</div>
	</div>
</div>
<?php
get_footer();