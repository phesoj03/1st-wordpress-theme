	
		<a id="back-to-top" title="Back to top"><span class="fas fa-angle-up"></span></a>

		<footer class="site-footer">
			<div class="container-fluid footer-container">
                <div class="row">
					<div class="col-md-12 footer-text text-center">
                        2018 &copy;<?php bloginfo( 'name' ) ?>, All Rights Reserved.
                    </div>
				</div>
			</div>
		</footer>
	
		<?php wp_footer() ?>
		<!-- <script>$(document).ready(function() { $('body').bootstrapMaterialDesign(); });</script> -->
	</body>
</html>